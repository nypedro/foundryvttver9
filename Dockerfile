FROM node:17-bullseye

RUN mkdir /foundry /foundry_data

COPY FoundryVTT-9.242.zip /foundry

RUN cd /foundry && \
    unzip FoundryVTT-9.242.zip

WORKDIR /foundry

CMD ["node", "resources/app/main.js", "--dataPath=/foundry_data"]
